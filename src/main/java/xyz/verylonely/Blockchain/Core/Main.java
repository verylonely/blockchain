package xyz.verylonely.Blockchain.Core;

import xyz.verylonely.Blockchain.Core.Blockchain.Chain;
import xyz.verylonely.Blockchain.Core.Blockchain.Mempool;
import xyz.verylonely.Blockchain.Core.Blockchain.Transaction;
import xyz.verylonely.Blockchain.Core.JsonEngine.JsonEngine;
import xyz.verylonely.Blockchain.Core.Wallet.Keygenerator;
import xyz.verylonely.Blockchain.Core.Wallet.Wallet;
import xyz.verylonely.Utils.Mining;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;


public class Main {
    public static Mempool mempool = new Mempool();

    public static Chain chain = new Chain();

    static boolean chainIsValid = true;

    static Mining miningThread = new Mining();

    static Keygenerator keygenerator;
    static Wallet wallet;

    static {
        try {
            wallet = new Wallet();
        } catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, SignatureException, InvalidKeyException, InvalidKeySpecException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        LoadChain();
        chainIsValid = chain.verify();
        System.out.println("Chain is valid: " + chainIsValid);
        wallet.CreateTransaction("sosi");
        miningThread.start();

        while (true)
        {
            String input = br.readLine();
            if(input.equals("stopmining"))
                miningThread.suspend();
            if(input.equals("startmining") && miningThread.isAlive())
                miningThread.resume();
        }

    }

    public static void Save() throws IOException {
        String filename = "./blocks/block"+chain.getLastBlockIndex() + ".json";
        JsonEngine.SaveBlockData(filename, chain.getBlock(chain.getLastBlockIndex()));
    }

    public static void LoadChain() throws IOException {
        int index = 0;
        try {
            while (true) {

                String filename = "./blocks/block" + index + ".json";

                chain.addBlock(JsonEngine.LoadBlockData(filename));
                //System.out.println(JsonEngine.GetBlockData(chain.getBlock(chain.getLastBlockIndex())));
                index++;
            }

        } catch (Exception e){
            if(index < 1)
                chain.GenerateGenesisBlock(); Save();
            System.out.println("Loaded " + index + " blocks");
        }

    }
}
