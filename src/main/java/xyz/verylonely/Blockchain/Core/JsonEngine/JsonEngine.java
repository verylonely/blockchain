package xyz.verylonely.Blockchain.Core.JsonEngine;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import xyz.verylonely.Blockchain.Core.Blockchain.Block;
import xyz.verylonely.Blockchain.Core.Blockchain.Transaction;

import java.io.File;
import java.io.IOException;
import java.util.List;

public abstract class JsonEngine
{
    static ObjectMapper objectMapper = new ObjectMapper();
    public static String GetBlockData(Block block) throws JsonProcessingException {

        return objectMapper.writeValueAsString(block);
    }

    public static String GetTransactionsData(Object[] transactionList) throws JsonProcessingException {

        return objectMapper.writeValueAsString(transactionList);
    }

    public static String GetTransactionData(Transaction tx) throws JsonProcessingException {
        return objectMapper.writeValueAsString(tx);
    }

    public static void SaveBlockData(String filename, Block block) throws IOException {
        objectMapper.writeValue(new File(filename), block);
    }

    public static Block LoadBlockData(String filename) throws IOException {
        return objectMapper.readValue(new File(filename), Block.class);
    }
}
