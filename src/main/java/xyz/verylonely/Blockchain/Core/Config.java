package xyz.verylonely.Blockchain.Core;

public abstract class Config
{
    public static final NetworkType NETWORK_TYPE = NetworkType.Testnet;

    public static final String HASH_ALGO  = "SHA-256";
    public static final int DIFF = 2;

    public enum NetworkType
    {
        Mainnet, Testnet
    }
}
