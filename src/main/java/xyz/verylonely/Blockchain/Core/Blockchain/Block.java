package xyz.verylonely.Blockchain.Core.Blockchain;

import xyz.verylonely.Blockchain.Core.Utils.HashCalculator;
import xyz.verylonely.Blockchain.Core.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static xyz.verylonely.Blockchain.Core.Main.mempool;

public class Block
{
    private int index;
    private String hash;
    private String prevHash;
    private long timestamp;
    private int nonce;
    //private List<Transaction> data;
    private List<String> transactions;


    public Block() {
    }

    public Block(int prevBlockIndex, String prevHash)
    {
        this.index = prevBlockIndex + 1;
        //this.data = new ArrayList<>();
        this.transactions = new ArrayList<>();
        this.hash = "";
        this.prevHash = prevHash;
        this.timestamp = System.currentTimeMillis() / 1000L;
        Init();

    }

    public List<String> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<String> transactions) {
        this.transactions = transactions;
    }

    public int getIndex() {
        return index;
    }

    public void setNonce(int value)
    {
        this.nonce = value;
    }

//    public Object[] getData()
//    {
//        return data.toArray();
//    }

    public void printBlock()
    {
        System.out.println("--------------------------");
        System.out.println("Index: " + this.index);
        System.out.println("Hash: " + this.hash);
        System.out.println("Prev hash: " + this.prevHash);
//        System.out.println("Transactions: " + this.data.size());
        System.out.println("Timestamp: " + this.timestamp);
        System.out.println("Nonce: " + this.nonce);
        System.out.println("--------------------------");
    }

    private void Init(){
        String _hash = null;
        while (!HashCalculator.checkSlice(_hash))
        {
            addTransactions(mempool.getPool());

            this.nonce++;
            _hash = HashCalculator.CalculateHash(Utils.getBlockBytes(this, true));
        }
        mempool.clear();
        this.hash = _hash;

    }

//    public void setData(List<Transaction> data) {
//        this.data = data;
//    }

    public long getTimestamp(boolean update) {
        if(update)
            this.timestamp = System.currentTimeMillis() / 1000L;
        return this.timestamp;
    }

    public long getTimestamp()
    {
        return this.timestamp;
    }

    public int getNonce() {
        return this.nonce;
    }

    public String getHash() {
        return this.hash;
    }

    public void addTransactions(List<String> tx)
    {
        for(int i=0; i<tx.size(); i++)
        {
            transactions.add(tx.get(i));
            tx.remove(i);
        }
    }

    public String getPrevHash() {
        return this.prevHash;
    }

    public boolean verify()
    {
        return this.hash.equals(HashCalculator.CalculateHash(Utils.getBlockBytes(this, false)));
    }
}
