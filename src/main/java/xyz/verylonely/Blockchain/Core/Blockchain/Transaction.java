package xyz.verylonely.Blockchain.Core.Blockchain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import xyz.verylonely.Blockchain.Core.JsonEngine.JsonEngine;
import xyz.verylonely.Blockchain.Core.Utils.Base58;
import xyz.verylonely.Blockchain.Core.Utils.HashCalculator;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class Transaction
{
    public Transaction(String base58) {
        Decode(base58);
    }

    private String hash;
    private String data;
    private long timestamp;
    private String senderPublicKey;
    private String signature;

    public Transaction(String data, String publicKey)
    {
        this.data = data;
        this.timestamp = System.currentTimeMillis() / 1000L;
        this.senderPublicKey = publicKey;
        this.hash = HashCalculator.CalculateHash(this);
    }

    public String getSenderPublicKey() {
        return senderPublicKey;
    }

    public String getHash() {
        return hash;
    }

    public String getData() {
        return data;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getSignature() {
        return signature;
    }

    public void printTx()
    {
        System.out.println("Transaction: ");
        System.out.println("Timestamp: "+ this.timestamp);
        System.out.println("Data: "+ this.data);
        System.out.println("Hash: "+ this.hash);
        System.out.println("Signature: " + this.signature);
    }

    @JsonIgnoreProperties
    public String Encoded() throws JsonProcessingException {
        String tx = JsonEngine.GetTransactionData(this);

        return Base58.INSTANCE.encode(tx.getBytes());
    }

    public void sign(KeyPair key) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature signature = Signature.getInstance("SHA256withECDSA");
        signature.initSign(key.getPrivate());
        signature.update(this.hash.getBytes());
        this.signature = Base58.INSTANCE.encode(signature.sign());
    }

    public boolean verify() throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, InvalidKeySpecException {
        Signature signature = Signature.getInstance("SHA256withECDSA");
        PublicKey key = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(Base58.INSTANCE.decode(senderPublicKey)));
        signature.initVerify(key);
        signature.update(this.hash.getBytes());
        if(signature.verify(Base58.INSTANCE.decode(this.signature)) && this.hash.equals(HashCalculator.CalculateHash(this)))
            return true;

        return false;

    }

    public void Decode(String encoded)
    {
        byte[] bytes = Base58.INSTANCE.decode(encoded);
        String json = new String(bytes, StandardCharsets.UTF_8);
    }
}
