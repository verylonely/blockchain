package xyz.verylonely.Blockchain.Core.Blockchain;

import java.util.ArrayList;
import java.util.List;

public class Mempool {
    private List<String> pool = new ArrayList<>();

    public void addTransaction(String tx)
    {
        pool.add(tx);
    }

    public List<String> getPool()
    {
        return pool;
    }

    public void clear()
    {
        pool.clear();
    }
}
