package xyz.verylonely.Blockchain.Core.Blockchain;

import java.util.ArrayList;
import java.util.List;

public class Chain {
    private List<Block> chain = new ArrayList<>();

    public Chain() {

    }

    public void GenerateGenesisBlock()
    {
        chain.add(new Block(-1, null));
    }

    public Block getBlock(int index)
    {
        return chain.get(index);
    }

    public void addBlock(Block block)
    {
        this.chain.add(block);
    }
    public int getLastBlockIndex()
    {
        return chain.size() - 1;
    }

    public void printChain()
    {
        System.out.println(chain.toString());
    }

    public boolean verify()
    {
        for (Block block : chain) {
            if (!block.verify())
                return false;
        }
        return true;
    }

}
