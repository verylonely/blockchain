package xyz.verylonely.Blockchain.Core.Wallet;

import xyz.verylonely.Blockchain.Core.Utils.Base58;
import java.security.*;
import java.security.spec.ECGenParameterSpec;

public class Keygenerator
{

    KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC");
    ECGenParameterSpec spec = new ECGenParameterSpec("secp256r1");
    KeyPair keyPair;
    PublicKey publicKey;
    PrivateKey privateKey;

    public Keygenerator() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        keyPairGenerator.initialize(spec);
        GenerateKeyPair();
    }

    public String getPublicKey() {
        return Base58.INSTANCE.encode(publicKey.getEncoded());
    }
    public String getPrivateKey() {
        return Base58.INSTANCE.encode(privateKey.getEncoded());
    }

    public void GenerateKeyPair()
    {
        keyPairGenerator.initialize(256);
        keyPair = keyPairGenerator.generateKeyPair();
        publicKey = keyPair.getPublic();
        privateKey = keyPair.getPrivate();
    }
}
