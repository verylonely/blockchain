package xyz.verylonely.Blockchain.Core.Wallet;

import com.fasterxml.jackson.core.JsonProcessingException;
import xyz.verylonely.Blockchain.Core.Blockchain.Transaction;
import xyz.verylonely.Blockchain.Core.Utils.Base58;

import java.security.*;
import java.security.spec.InvalidKeySpecException;

import static xyz.verylonely.Blockchain.Core.Main.mempool;

public class Wallet {
    Keygenerator keygenerator;

    public Wallet() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
        keygenerator = new Keygenerator();
    }

    public String getAddress()
    {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        byte[] digest = md.digest(keygenerator.publicKey.getEncoded());
        return Base58.INSTANCE.encode(digest);
    }

    public void CreateTransaction(String data) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException, JsonProcessingException {
        Transaction tx = new Transaction(data, keygenerator.getPublicKey());
        tx.sign(keygenerator.keyPair);

        mempool.addTransaction(tx.Encoded());
    }

}
