package xyz.verylonely.Blockchain.Core.Utils;

import xyz.verylonely.Blockchain.Core.Blockchain.Block;
import xyz.verylonely.Blockchain.Core.Blockchain.Transaction;

import java.util.Arrays;

public abstract class Utils {
    public static void printvl(String string)
    {
        System.out.println(string);
    }

    public static byte[] getBlockBytes(Block block, boolean updateTimeStamp)
    {
        String blockData = block.getIndex() + "-" + block.getNonce() + "-" +  block.getTimestamp(updateTimeStamp) + "-" + block.getTransactions()  + "-" + block.getPrevHash();

        return blockData.getBytes();
    }

    public static byte[] getTxBytes(Transaction tx) {
        String data = (tx.getData() + "-" + tx.getTimestamp() + tx.getSenderPublicKey());
        return data.getBytes();
    }


}
