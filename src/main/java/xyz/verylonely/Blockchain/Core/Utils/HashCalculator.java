package xyz.verylonely.Blockchain.Core.Utils;

import xyz.verylonely.Blockchain.Core.Config;
import xyz.verylonely.Blockchain.Core.Blockchain.Transaction;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import static xyz.verylonely.Blockchain.Core.Utils.Utils.getTxBytes;

public abstract class HashCalculator {
    public static String CalculateHash(byte[] data)
    {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance(Config.HASH_ALGO);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        byte[] digest = null;


        digest = md.digest(data);

        return Base58.INSTANCE.encode(digest);
    }

    public static String CalculateHash(Transaction tx)
    {
        byte[] bytesOfData = getTxBytes(tx);

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance(Config.HASH_ALGO);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Base58.INSTANCE.encode(md.digest(bytesOfData));
    }

    public static boolean checkSlice(String _data)
    {

        if(_data == null)
            return false;

        byte[] data = Base58.INSTANCE.decode(_data);

        for(int i=0; i < Config.DIFF ;i++)
        {
            if(data[i] != 0)
                return false;
        }

        return true;
    }


}

