package xyz.verylonely.Utils;

import xyz.verylonely.Blockchain.Core.Blockchain.Block;
import xyz.verylonely.Blockchain.Core.Blockchain.Chain;
import xyz.verylonely.Blockchain.Core.Main;
import java.io.IOException;

public class Mining extends Thread
{
    private boolean enabled = false;
    private boolean chainIsValid;

    Chain chain = Main.chain;

    public void run() {
        chainIsValid = chain.verify();
        this.suspend();
        while (chainIsValid)
        {
            Block newBLock = new Block(chain.getLastBlockIndex(), chain.getBlock(chain.getLastBlockIndex()).getHash());
            chain.addBlock(newBLock);
            System.out.println("Block found: " + newBLock.getIndex() + " hash " + newBLock.getHash());
            try {
                Main.Save();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void Disable()
    {
        enabled = false;
    }

    public void Enable()
    {
        enabled = true;
    }

}
